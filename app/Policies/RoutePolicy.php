<?php

namespace App\Policies;

use App\User;
use App\Route;
use Illuminate\Auth\Access\HandlesAuthorization;
use Auth;

class RoutePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(User $user, Route $route)
    {   

        if (Auth::user()->role === 1) {
            return true;
        }

        if ($user->id === $route->user_id) {
            return true;
        }

        return false;
        
    }
}
