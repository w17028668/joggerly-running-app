<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-left" href="#">Joggerly</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="{{route('create.route')}}">Create Routes</a></li>
      <li><a href="#">My profile</a></li>
    </ul>
  </div>
</nav>