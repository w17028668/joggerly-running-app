@extends('layouts.app')
@section('content')
<div class="container" id="view-route-page">
    <div class="row">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-11">
                    <h4>{{ $route->name }}</h4>
                </div>
                <div class="col-md-1">
                    <span id="flag">
                        <i class="fas fa-flag fa-2x" style="margin-top: 18px;"></i>
                    </span>
                </div>
            </div>
            <hr>
            <p>
                {{ $route->description }}
            </p>

            <div class="row">
                <div class="col-md-6">
                    Address: <b>{{ $route->address }}</b>
                </div>
                <div class="col-md-6">
                    City: <b>{{ $route->city->name }}</b>
                </div>
                <div class="col-md-6">
                    Distance: <b>{{ $route->distance }}</b>
                </div>
                <div class="col-md-6">
                    Terrain: <b>{{ $route->terrain->type }}</b>
                </div>
                <div class="col-md-6">
                    Rating: 
                    @for($i = 0; $i < 5; $i++)
                      @if($i >= $route->rating)
                        <i class="far fa-star"></i>
                      @else
                        <i class="fa fa-star"></i>
                      @endif
                    @endfor
                </div>
            </div>

            <div class="row" style="margin-top: 127px;">
                <div class="col-md-3">
                    <a href="{{ route('get.route') }}"class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
                @can('update', $route)
                <div class="offset-md-4 col-md-2" style="display: inherit;">
                    <a class="btn btn-primary btn-sm" href="{{ route('edit.route', $route->id) }}">Edit</a>
                    <form action="{{ route('delete.route', $route->id) }}" method="post">
                        @csrf
                        <button type="submit" name="delete" class="btn btn-danger btn-sm" style="margin:0 5px;">Delete</button>
                    </form>
                </div>
                @endcan
            </div>

        </div>
        <div class="col-md-7">
            <div style="height: 350px; background: #e6e6e6;">
                <div id="map" style="height: 100%"> </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="reviews">
        <h4>Reviews</h4>
        @if(count($reviews) !== 0)
            @foreach($reviews as $review)
                <div class="card">
                    <div class="card-header">
                        @for($i = 0; $i < 5; $i++)
                            @if($i >= $review->rating)
                                <i class="far fa-star"></i>
                            @else
                                <i class="fa fa-star"></i>
                            @endif
                        @endfor
                        by {{ $review->user->name }}
                    </div>
                    <div class="card-body">
                        {{ $review->review}}
                    </div>
                    <span class="flag2" data-id="{{$review->id}}">
                      <i class="fas fa-flag fa-1x" style="margin-top: 18px; margin-left: 18px;"></i>
                    </span>
                </div>
            @endforeach
        @else
            <p>
                This route has no reviews yet!
            </p>
        @endif

        @if(Auth::user()->verified === 1)
        <div class="offset-md-3 col-md-6 leave-review">
            <h4>Leave a review</h4>
            <form method="post" action="{{ route('create.review', $route->id) }}" enctype="multipart/form-data" autocomplete="off">
              @csrf
                @if ($errors->any())
                  <div class="alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                @if (session()->has('message'))
                  <div class="alert-success">
                    {{ session()->get('message') }}
                  </div>
                @endif
                <div class="form-group">
                  <textarea rows="4" cols="50" name="review" style="resize:none;" class="form-control"> 
                  </textarea>
                </div>
                
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="number" step="1"  min="0" max="5" name="rating" placeholder="Rating out of 5" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" style="width: 100%;">Publish</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @endif
    </div>
</div>

@endsection


@section('scripts')
  <script>

      var map, polyline, markers = new Array();
      var infoWindow, directionsDisplay, directionsService, polyLengthInMeters, duration;
      var routeObj = {!! json_encode($route->toArray()) !!};
      var positionsArr = JSON.parse(routeObj.route);
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var labelIndex = 0;

      $(document).ready(function(){
        $('#flag').click(function(e){
          e.preventDefault();
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            url: "{{ url('/route/{route}') }}",
            method: 'POST',
            data: {
              route_id:  {!! json_encode($route->id) !!},
              review_id:  null
            },
            success: function(result){
              $("#flag").css("color", "red");
              $("#flag").off("click");   
            }});
          });

          $('.flag2').click(function(e){
            $(this).css("color", "red");
            $(this).off("click");   
            e.preventDefault();
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ url('/route/{route}') }}",
              method: 'POST',
              data: {
                route_id:  null,
                review_id:  $(this).data('id')
              },
              success: function(result){
              
              }});
          });
      });


    

    

      function initMap() {
        console.log("INIT");
        var mapOptions = {
          center: positionsArr[0],
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        infoWindow = new google.maps.InfoWindow;
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer;

        map = new google.maps.Map(document.getElementById('map'),
        mapOptions);

        var route = new google.maps.Polyline({
          path: positionsArr,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });

        route.setMap(map);
        addMarkers(positionsArr);
      
      }
      
      function addMarkers(markers){
        for (var i = 0; i < markers.length; i++) {
          
          var marker = new google.maps.Marker({
              position: markers[i],
              label: labels[labelIndex++ % labels.length],
          });

          marker.setMap(map);
        }
      }

      initMap();
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw&libraries=geometry&callback=initMap">
    </script>
@endsection

    

