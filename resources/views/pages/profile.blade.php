@extends('layouts.app')
@section('content')


<div class="container emp-profile">
    <div class="row">
        <div class="col-md-4">
            <div class="profile-img">
                <img src="{{ asset('images/profile.jpg') }}" alt=""/ class="pro">
                <div class="file btn btn-lg btn-primary">
                    Change Photo
                    <input type="file" name="file"/>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="profile-head">
                <h5>
                    {{$user->name}}
                </h5>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#routes" role="tab" aria-controls="profile" aria-selected="false">Routes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="profile" aria-selected="false">Reviews</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#events" role="tab" aria-controls="profile" aria-selected="false">Events</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$user->name}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-6">
                                    <p>{{$user->email}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="routes" role="tabpanel" aria-labelledby="profile-tab">
                            @foreach($routes as $route)
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Route</label>
                                </div>
                                <div class="col-md-4">
                                    <a href="route/{{$route->id}}"><p>{{$route->name}}</p></a>
                                </div>
                                <div class="col-md-4">
                                    <p>{{$route->reviews}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="profile-tab">
                            @foreach($reviews as $review)
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Review</label>
                                </div>
                                <div class="col-md-3">
                                    <p>{{$review->review}}</p>
                                </div>
                                <div class="col-md-3">
                                    <p>{{$review->route->name}}</p>
                                </div>
                                <div class="col-md-3">
                                    @for($i = 0; $i < 5; $i++)
                                        @if($i >= $review->rating)
                                            <i class="far fa-star"></i>
                                        @else
                                            <i class="fa fa-star"></i>
                                        @endif
                                    @endfor
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="events" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-4">
                                    <p>Event</p>
                                </div>
                                <div class="col-md-4">
                                    <p>26/08/19</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
        <div class="col-md-2">
            <input type="submit" class="btn btn-sm btn-primary" name="btnAddMore" value="Edit Profile"/>
        </div>
    </div>
</div>
@endsection