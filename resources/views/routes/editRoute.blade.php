@extends('layouts.app')
@section('content')
    <div class="container" style="height: 400px; width: 800px; margin-bottom: 30px;">
      <div id="map" style="height: 100%"> </div>
    </div>
    <div class="container">
      <div class="inputs-container">
        <form method="post" action="{{ route('edit.route.post', $route->id) }}" enctype="multipart/form-data" autocomplete="off">
          {{ csrf_field() }}
          @if ($errors->any())
            <div class="alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                 @endforeach
              </ul>
            </div>
          @endif
          @if (session()->has('message'))
            <div class="alert-success">
              {{ session()->get('message') }}
            </div>
          @endif
          <input type="hidden" id="route" name="route" value="{{$route->route}}" readonly>
          <div class="form-group">
            <input name="name" type="text" class="form-control" id="nameID" placeholder="Route Name" class="form-control" value="{{$route->name}}">
          </div>
          <div class="address-container">
            <div class="form-group">
              <input name="address" type="text" class="form-control" id="addressID" placeholder="Route Address" class="form-control" value="{{$route->address}}">
            </div>
            <div class="form-group">
              <select name="city_id" class="form-control">
                <option value="{{$route->city->id}}">{{$route->city->name}}</option>
                  @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                  @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <input type="text" id="distance" name="distance" value="{{$route->distance}}" class="form-control" placeholder="Distance in meters" readonly>
          </div>
          <div class="form-group">
            <select name="terrain_id" class="form-control">
              <option value="{{$route->terrain->id}}">{{$route->terrain->type}}</option>
                @foreach($terrains as $terrain)
                  <option value="{{$terrain->id}}">{{$terrain->type}}</option>
                @endforeach
            </select>
          </div>
          <div class="form-group">
            <textarea name="description" rows="4" cols="50"  class="form-control" style="resize:none">{{$route->description}}</textarea>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>
    </div>
    @endsection

    @section('scripts')
    <script>
    
    var map, polyline, markers = new Array();
      var infoWindow, directionsDisplay, directionsService, polyLengthInMeters, duration;
      var routeObj = {!! json_encode($route->toArray()) !!};
      var positionsArr = JSON.parse(routeObj.route);
      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var labelIndex = 0;
    

      function initMap() {
        var mapOptions = {
          center: positionsArr[0],
          zoom: 14.5,
          mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        infoWindow = new google.maps.InfoWindow;
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer;

        map = new google.maps.Map(document.getElementById('map'),
        mapOptions);

        polyline = new google.maps.Polyline({
            path: positionsArr,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });

        polyline.setMap(map);
        addMarkers(positionsArr);
        
        google.maps.event.addListener(map, 'click', function (event) {
          addPoint(event.latLng);
        });

      }
      
      function addMarkers(array){
        for (var i = 0; i < array.length; i++) {
          
          var marker = new google.maps.Marker({
              position: array[i],
              label: labels[labelIndex++ % labels.length],
          });
          
          marker.setMap(map);
         
          deleteMarker(marker); 
          markers.push(marker);
          console.log(markers);

        }
      }

      function deleteMarker(marker){
        marker.addListener('dblclick', function() {
            removePoint(marker);
          });
          
      }

      function removePoint(marker) {
      
        for (var i = 0; i < markers.length; i++) {

          if (markers[i] === marker) {

            markers[i].setMap(null);
            markers.splice(i, 1);
            
            positionsArr.splice(i, 1);
            document.getElementById("route").value =  JSON.stringify(positionsArr);


            polyline.getPath().removeAt(i);
          }
        }
        
        polyLengthInMeters = google.maps.geometry.spherical.computeLength(polyline.getPath().getArray());
        document.getElementById("distance").value =  Math.ceil(polyLengthInMeters);
    }

    function addPoint(latlng) {

      var marker = new google.maps.Marker({
        position: latlng,
        map: map
      });

      markers.push(marker);

      positionsArr.push(marker.position);
      document.getElementById("route").value =  JSON.stringify(positionsArr);

      polyline.getPath().setAt(markers.length - 1, latlng);

      polyLengthInMeters = google.maps.geometry.spherical.computeLength(polyline.getPath().getArray());
      document.getElementById("distance").value =  Math.ceil(polyLengthInMeters);

      /*
      for(var i = 0; i < markers.length; i++){
        calculateAndDisplayRoute(directionsService, directionsDisplay, i);
        console.log(duration);
      }*/

      marker.addListener('dblclick', function() {
        removePoint(marker);
      });
    }

      initMap();
      
 
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw&sensor=false&libraries=geometry&callback=initMap">
    </script>
@endsection
