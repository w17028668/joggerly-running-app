<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->integer('distance');
            $table->integer('terrain_id')->unsigned();
            $table->unsignedInteger('city_id');
            $table->double('rating')->nullable();
            $table->integer('user_id')->unsigned();
            $table->string('description');
            $table->text('route');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('terrain_id')->references('id')->on('terrains');
            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
