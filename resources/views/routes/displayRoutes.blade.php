@extends('layouts.app')
@section('content')   
<div class="container" id="search-routes-page">
    <div class="row routes-search-section">
        <div class="col-md-12">
            <h1>Search for Routes</h1>
            <p>
                You can find routes anywhere around the world! Where would you like to run?
            </p>
            <form method="post" action="{{ route('get.route') }}" enctype="multipart/form-data" autocomplete="off">
                @csrf
                @if ($errors->any())
                    <div class="alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session()->has('message'))
                    <div class="alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="row">
                    <div class="offset-md-3 col-md-5">
                        <select class="form-control" name="name" id="cityValue">
                            <option value="" selected disabled hidden>City</option>
                            @foreach($cities as $city)
                                <option value="{{$city->id}}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button id="getRoutes" class="btn btn-primary">Search</button>
                    </div>
                </div>    
            </form>
        </div>
    </div>
    <hr>
    <div class="row" id="routes-filter">
        <div class="offset-md-8 col-md-4">
            <form method="post" action="{{ route('filter.route') }}" enctype="multipart/form-data" autocomplete="off">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <input type="hidden" id="city" name="city" value="{{$id}}">    
                        <select name="filter" class="form-control">
                            <option value="" selected disabled hidden>Filter</option>
                            <option value="1">Shortest</option>
                            <option value="2">Longest</option>
                            <option value="3">Rating</option>
                        </select>    
                    </div>
                    <div class="col-4">
                        <button id="getRoutes" class="btn btn-primary">Filter</button>
                    </div>
                </div>
                
            </form>
        </div>
    </div>

    <div class="row" id="routes-display">
        @foreach($routes as $route)
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <img src="<?php echo asset('/images/image_' . random_int(1, 8) . '.jpg') ?>" alt="">
                    </div>
                    <div class="card-body">
                        Address: <b>{{ $route->address }}</b> <br>
                        City: <b>{{ $route->city->name}}</b> <br>
                        Distance: <b>{{ $route->distance }} metres</b><br>
                        Rating: 
                        @for($i = 0; $i < 5; $i++)
                            @if($i >= $route->rating)
                                <i class="far fa-star"></i>
                            @else
                                <i class="fa fa-star"></i>
                            @endif
                        @endfor
                        <div class="col-md-12 route-view-button">
                            <a class="btn btn-primary btn-sm" href="{{ route('show.route', $route->id) }}">View</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
@section('scripts')
<script>
  
    $( document ).ready(function() {
        $('#cityValue').on('change', function() {
            $('#city').val(this.value);
            console.log(  $('#city').val());
        });
    });
   
</script>
@endsection
