<!DOCTYPE html>
<html>
  <head>
    <title>Geolocation</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
    
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <div>
        <ul class="list-group">
            @foreach($reviewFlags as $flag)
                    <li>{{$flag->review}}
                        <ul>
                            <li>{{$flag->review['id']}}</li>
                            <li>{{$flag->review['review']}}</li>
                        </ul>
                    </li>
            @endforeach
        </ul>
        <ul class="list-group">
            @foreach($routeFlags as $flag)
                    <li>{{$flag->route}}</li>
            @endforeach
        </ul>
    </div>  
  </body>
</html>
