@extends('layouts.app')
@section('content')
<div class="container" style="padding-bottom: 25px;">
    <nav class="navbar navbar-expand-lg navbar-light bg-admin">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('admin.users') }}">Users <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.flags') }}" class="nav-link">Flags</a>
                    </li>
                </ul>
            </div>
    </nav>
</div>

<div class="container" id="admin-users-page">
    <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-5">
						<h2>Flags management</h2>
					</div>
                </div>
            </div>
            <h4>Route flags</h4>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Route name</th>	
						<th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($routeFlags as $key => $flag)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td><a href="{{ route('show.route', ['route' => $flag->route->id]) }}">{{ $flag->route->name }}</a></td>
                        <td>{{ $flag->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <h4>Review flags</h4>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Review</th>						
						<th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($reviewFlags as $key => $flag)
                    <tr>
                        <td>{{ $key + 1 }}</td>
<<<<<<< HEAD
                        <td>{{ $flag->review->review }}</td>
=======
                        <td><a href="{{ route('show.route', ['route' => $flag->route->id]) }}">{{ $flag->route->name }}</a></td>
>>>>>>> 4239ba6bf3622d8c3ccc617569f711aa2eca88be
                        <td>{{ $flag->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection