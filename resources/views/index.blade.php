@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="menu-container">
            <img src="{{ asset('images/joggers.jpg') }}" alt="" class="hero-img">
            <div class="info-container">
                <img src="{{ asset('/images/logo.png') }}" alt="" style="margin-bottom: -40px;">
                <h1 class="header">Search for routes</h1>
                <p class="opener">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a href="#about"><input type="button" value="About us" class="button"></a>
            </div>
        </div>
        <div id="about">
            <div class="row">
                <div class="col-md-6">
                    <h1>Search for routes</h1>
                    <p class="info-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="{{route('display.routes')}}"><input type="button" value="Search for routes" class="button"></a>
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('images/about.jpg') }}" alt="" style="width: 100%;">
                </div>
            </div>
        </div>
    </div>
@endsection