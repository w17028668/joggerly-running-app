<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public function index()
    {   
        $user = Auth::user();
        $routes = $user->routes;
        $reviews = $user->reviews()->with('route')->get();


        return view('pages.profile', array('user' => $user, 'routes' => $routes, 'reviews' => $reviews));
    }
}
