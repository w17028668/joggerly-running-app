@extends('layouts.app')
@section('content')
<div class="container" style="padding-bottom: 25px;">
    <nav class="navbar navbar-expand-lg navbar-light bg-admin">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('admin.users') }}">Users <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.flags') }}" class="nav-link">Flags</a>
                    </li>
                </ul>
            </div>
    </nav>
</div>
<div class="container">
    <a href="{{ route('admin.user', ['user' => $user->id]) }}" class="btn btn-success btn-sm"><i class="fas fa-arrow-left"></i> Back</a>
    <hr>
    <h4>{{ $user->name }} password management</h4>
    <div class="row">
        <div class="col-5">
            @if (session('status') == 'OK')
                <div class="alert alert-success">
                    User's <b>{{ $user->name}}</b> password was changed sucessfully!
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif

            <form method="POST" action="{{ route('admin.user.password.change', ['user' => $user]) }}">
                @csrf
                <div class="form-group">
                    <label for="password">New password</label>
                    <input id="password" name="password" type="password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm new password</label>
                    <input id="password_confirmation" name="password_confirmation" type="password" class="form-control">
                </div>
                <button class="btn btn-success" type="submit">Confirm</button>
            </form>
        </div>
    </div>
</div>
@endsection