<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Route;
use App\Flag;
use App\User;

class Review extends Model
{
    //
    protected $fillable = [
        'user_id', 'route_id', 'review', 'rating'
    ];

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function flags()
    {
        return $this->hasMany(Flag::class);
    }
}
