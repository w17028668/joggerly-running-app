<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Route;

class City extends Model
{
    public function routes() 
    {
        return $this->hasMany(Route::class);
    }
}
