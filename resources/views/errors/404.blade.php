<!DOCTYPE html>
<html>
<head>
    <title>Joggerly - 404 - Page not found</title>
</head>
<body style="background-color: #1abc9c;">
    <div style="display: flex; align-items: center; justify-content: center; height: 100vh">
        <img src="/images/logo.png"" alt="" >
        <h1 style="color: #fff">Page not found | 404</h1>
    </div>
</body>
</html>