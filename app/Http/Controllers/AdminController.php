<?php

namespace App\Http\Controllers;

use App\User;
use App\Flag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    /**
     * Page that returns a view to a admin/index page.
     * Was later modified into instantly showing users page.
     */
    public function index()
    {
        return view('admin.users', ['users' => \App\User::all()]);
    }

    /**
     * Returns a view with users and passes $users variable.
     */
    public function users()
    {
        return view('admin.users', ['users' => \App\User::all()]);
    }

    /**
     * Returns a view of a single user and passes $user variable.
     */
    public function user(User $user)
    {
        return view('admin.user', ['user' => $user]);
    }

    /**
     * Returns a view where a form for changing password is. 
     */
    public function managePassword(User $user)
    {
        return view('admin.user_password', ['user' => $user]);
    }

    /**
     * Change password functionlity. Validates the request and then updates
     * user's password to a new one. After completion, redirects to password
     * change page with a message.
     */
    public function changePassword(Request $request, User $user)
    {
        $request->validate([
            'password' => 'required|confirmed|min:6'
        ]);

        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('admin.user.password', ['user' => $user])->with('status', 'OK');
    }

    /**
     * Verifies user and redirects to user view with a message.
     */
    public function verify(User $user)
    {
        $user->verified = true;
        $user->save();

        return redirect()->route('admin.user', ['user' => $user])->with('status', 'OK');
    }

    /**
     * Suspends user and redirects to user view with a message.
     */
    public function suspend(User $user)
    {
        $user->verified = false;
        $user->save();

        return redirect()->route('admin.user', ['user' => $user])->with('status', 'SUSPENDED');
    }

    /**
     * Changes user's role and redirects to user view with a message.
     */
    public function changeRole(User $user)
    {
        $user->role = request('role');
        $user->save();

        return redirect()->route('admin.user', ['user' => $user])->with('status', 'ROLE_CHANGED');
    }

    /**
     * Deletes user and redirects to users page with a status.
     */
    public function delete(User $user)
    {
        if($user->id == Auth::user()->id)   // in case front-end validation is breached
            return redirect()->route('admin.user', ['user' => $user])->with('status', 'DELETE_FAIL');
        else
        {   
            $user->reviews()->delete();
            $user->routes()->delete();
            $user->delete();
            return redirect()->route('admin.users', ['users' => \App\User::all()])->with('deletedUserName', $user->name);        
        }
    }

    /**
     * Returns a view of email creation form.
     */
    public function emailIndex(User $user)
    {
        return view('admin.create_email', ['user' => $user]);
    }

    /**
     * Composes an email and validates it before sending it.
     * Redirects back after finished with the status.
     */
    public function emailCompose(Request $request, User $user)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        $title = $request->get('title');
        $content = $request->get('content');

        Mail::send('email', ['title' => $title, 'content' => $content], function ($message) use ($user)
        {
            $message->from(Auth::user()->name . '@gmail.com', Auth::user()->name);
            $message->to($user->name.'@gmail.com');
        });

        return redirect()->route('admin.user', ['user' => $user])->with('status', 'EMAIL_SENT');
    }

    /**
     * Returns a view with all flags.
     */
    public function getFlags()
    {
        $reviewFlags = Flag::where('route_id', null)->with('review')->get();
        $routeFlags = Flag::where('review_id', null)->with('route')->get();

        return view('admin.flags', ['routeFlags' => $routeFlags, 'reviewFlags' => $reviewFlags]);
    }

}
