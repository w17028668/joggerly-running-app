<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use Auth;
use App\Route;

class ReviewController extends Controller
{
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Route $route)
    {
        //
        $request->validate([
            'review' => 'required', 
            'rating' => 'required|numeric|between:0,5', 
        ]);
         
        $total =  $route->reviews()->count();
        $sum = $route->reviews()->pluck('rating')->sum();
        $sum = $sum + $request->rating;
        $newRating = $sum/($total + 1);

        $request->request->add(['user_id' =>  Auth::id(), 'route_id' => $route->id]);
        Review::create($request->all());
        $route->rating = $newRating;
        $route->save();

        return back()->with('message', 'Your comment was created successfully!');;    
    }



}
