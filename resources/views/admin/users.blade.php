@extends('layouts.app')
@section('content')
<div class="container" style="padding-bottom: 25px;">
    <nav class="navbar navbar-expand-lg navbar-light bg-admin">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('admin.users') }}">Users <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.flags') }}" class="nav-link">Flags</a>
                    </li>
                </ul>
            </div>
    </nav>
</div>

<div class="container" id="admin-users-page">
    @if (session('deletedUserName'))
        <div class="alert alert-success">
            User <b>{{ session('deletedUserName') }}</b> was deleted successfully!
        </div>
    @endif
    <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-5">
						<h2>User Management</h2>
					</div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>						
						<th>Date Created</th>
						<th>Role</th>
						<th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $user['name'] }}</td>
                        <td>{{ date('d-m-Y', strtotime($user['created_at'])) }}</td>              
                        <td>
                            @if($user->role === 1)
                                <div class="badge badge-success">Administrator</div>
                            @elseif($user->role == 0)
                                <div class="badge badge-warning">User</div>
                            @else
                                <div class="badge badge-info">Committee member</div>
                            @endif
                        </td>
						<td>
							<a href="{{ route('admin.user', ['user' => $user->id ]) }}" class="settings" title="Settings" data-toggle="tooltip"><i class="fa fa-user-cog"></i> View</a>
						</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection