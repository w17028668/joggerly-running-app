<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Terrain;
use App\City;
use App\Review;
use App\User;
use App\Flag;

class Route extends Model
{
    //
    protected $fillable = [
      'name', 'address', 'terrain_id', 'description', 'distance', 'route', 'city_id', 'user_id'
    ];

    public function terrain()
    {
      return $this->belongsTo(Terrain::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function flags()
    {
        return $this->hasMany(Flag::class);
    }

}
