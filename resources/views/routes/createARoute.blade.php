@extends('layouts.app')
@section('content')
    <div class="container" style="height: 400px; width: 800px; margin-bottom: 30px;">
      <div id="map" style="height: 100%"> </div>
    </div>
    <div class="container">
      <div class="inputs-container">
        <form method="post" action="{{ route('create.route.post') }}" enctype="multipart/form-data" autocomplete="off">
          {{ csrf_field() }}
          @if ($errors->any())
            <div class="alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                 @endforeach
              </ul>
            </div>
          @endif
          @if (session()->has('message'))
            <div class="alert-success">
              {{ session()->get('message') }}
            </div>
          @endif
          <input type='hidden' id="route" name="route" readonly>
          <div class="form-group">
            <input name="name" type="text" class="form-control" id="nameID" placeholder="Route Name">
          </div>
          <div class="address-container">
            <div class="form-group">
              <input name="address" type="text" class="form-control" id="addressID" placeholder="Route Address">
            </div>
            <div class="form-group">
              <select name="city_id" class="form-control">
                <option value="" selected disabled hidden>City</option>
                  @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                  @endforeach
              </select>
            </div>
          </div>  
          <div class="form-group">
            <input type="text" id="distance" name="distance" placeholder="Distance in meters" class="form-control" readonly>
          </div>
          <div class="form-group">
            <select name="terrain_id" class="form-control">
              <option value="" selected disabled hidden>Type of terrain</option>
                @foreach($terrains as $terrain)
                  <option value="{{$terrain->id}}">{{$terrain->type}}</option>
                @endforeach
            </select>
          </div>
          <div class="form-group">
            <textarea name="description" rows="4" cols="50" style="resize:none"  class="form-control"></textarea>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Create Route</button>
          </div>
        </form>
      </div>
    </div>
@endsection


@section('scripts')
    <script>
      var map, polyline, markers = new Array();
      var infoWindow, directionsDisplay, directionsService, polyLengthInMeters, duration;
      var positionsArr = [];

      function initMap() {

        var mapOptions = {
          center: {lat: -34.397, lng: 150.644},
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.TERRAIN
        };
        infoWindow = new google.maps.InfoWindow;
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer;

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
        }, function() {
          handleLocationError(true, infoWindow, map.getCenter());
        });
        } else {
        // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }

        map = new google.maps.Map(document.getElementById('map'),
        mapOptions);

        polyline = new google.maps.Polyline({
          strokeColor: 'red',
          strokeWeight: 1,
          map: map
        });

        google.maps.event.addListener(map, 'click', function (event) {
          addPoint(event.latLng);
        });
      }

      function removePoint(marker) {
      
        for (var i = 0; i < markers.length; i++) {

          if (markers[i] === marker) {

            markers[i].setMap(null);
            markers.splice(i, 1);

            positionsArr.splice(i, 1);
            document.getElementById("route").value =  JSON.stringify(positionsArr);

            polyline.getPath().removeAt(i);
          }
        }

        polyLengthInMeters = google.maps.geometry.spherical.computeLength(polyline.getPath().getArray());
        document.getElementById("distance").value =  Math.ceil(polyLengthInMeters);
      }

      function addPoint(latlng) {

        var marker = new google.maps.Marker({
          position: latlng,
          map: map
        });

        markers.push(marker);

        positionsArr.push(marker.position);
        document.getElementById("route").value =  JSON.stringify(positionsArr);

        polyline.getPath().setAt(markers.length - 1, latlng);

        polyLengthInMeters = google.maps.geometry.spherical.computeLength(polyline.getPath().getArray());
        document.getElementById("distance").value =  Math.ceil(polyLengthInMeters);

        marker.addListener('dblclick', function() {
          removePoint(marker);
        });
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
          'Error: The Geolocation service failed.' :
          'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
     
    
      initMap();
      </script>
      <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw&sensor=false&libraries=geometry&callback=initMap">
    </script>
@endsection
   

