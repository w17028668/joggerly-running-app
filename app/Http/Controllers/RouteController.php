<?php

namespace App\Http\Controllers;

use App\Route;
use Illuminate\Http\Request;
use App\Terrain;
use App\City;
use Log;
use Auth;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $terrains = Terrain::get();
        $cities = City::get();
        return view('routes/createARoute', array('terrains' => $terrains,'cities' => $cities));
    }

    public function displayRoutes(Request $request){

        
        $routes = Route::get();  
        $cities = City::get();
     
        
        
        return view('routes/displayRoutes', array('routes' => $routes,'cities' => $cities, 'id' => null));
    }

    public function getRoutes(Request $request){
     
        if($request->name == null){

            $routes = Route::get();
            $cities = City::get();
                
            return view('routes/displayRoutes', array('routes' => $routes,'cities' => $cities, 'id' => null));

        }else{

            $routes = City::find( $request->name)->routes;
            $cities = City::get();
                
            return view('routes/displayRoutes', array('routes' => $routes,'cities' => $cities, 'id' => $request->name));
        }
        
    


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        //
        $request->validate([
            'name' => 'required|unique:routes,name', 
            'address' => 'required', 
            'terrain_id' => 'required',
            'city_id' => 'required', 
            'description' => 'required',
            'route' => 'required|min:3',
            'distance' => 'required|min:2',
        ],
        [
            'name.required' => 'Please choose a name for your route.',
            'address.required' => 'Please choose a start address for your route.',
            'terrain_id.required' => 'Please choose one of the terrains.',
            'city_id.required' => 'Please choose a city.',
            'description.required' => 'Please type a description about the route.',
            'route.required' => 'Please map your route.',
            'distance.required' => 'Please make sure you have at least two points.',
            'distance.min' => 'Please make sure you have at least two points.',
            'route.min' => 'Please map your route.'
        ]);

        $request->request->add(['user_id' =>  Auth::id()]);
        Route::create($request->all());

        return back()->with('message', 'Your route was created successfully!');    
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function show(Route $route)
    {
        $reviews = $route->reviews()->with('user')->latest()->get();
    
        return view('routes/showRoute', array('route' => $route,'reviews' => $reviews ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function edit(Route $route)
    {
          $terrains = Terrain::get();
          $cities = City::get();
          return view('routes/editRoute',  array('route' => $route, 'terrains' => $terrains,'cities' => $cities));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Route $route)
    {   
        $request->validate([
            'name' => 'required', 
            'address' => 'required', 
            'terrain_id' => 'required',
            'city_id' => 'required', 
            'description' => 'required',
            'route' => 'required|min:3',
            'distance' => 'required|min:2',
        ],
        [
            'name.required' => 'Please choose a name for your route.',
            'address.required' => 'Please choose a start address for your route.',
            'terrain_id.required' => 'Please choose one of the terrains.',
            'city_id.required' => 'Please choose a city.',
            'description.required' => 'Please type a description about the route.',
            'route.required' => 'Please map your route.',
            'distance.required' => 'Please map your route.',
            'distance.min' => 'Please map your route.',
            'route.min' => 'Please map your route.'
        ]);

        $route->update($request->all());

        return back()->with('message', 'Your route has been updated!');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function destroy(Route $route)
    {
        //
        $route->delete();
         

        
        
        $routes = Route::get();

        return redirect()->route('display.routes')->with('message', 'Your route has been deleted!');

    }

    public function filterRoutes(Request $request){



        if($request['filter'] == 1){

            if($request['city'] == null){
                $routes = Route::get()->sortBy('distance');
            }else{
                $routes = City::find( $request->city)->routes->sortBy('distance');
            }
            $cities = City::get();
            return view('routes/displayRoutes', array('routes' => $routes,'cities' => $cities, 'id' => $request->city));

        }else if($request['filter'] == 2){

            if($request['city'] == null){
                $routes = Route::get()->sortByDesc('distance');
            }else{
                $routes = City::find( $request->city)->routes->sortByDesc('distance');
            }
            $cities = City::get();
            return view('routes/displayRoutes', array('routes' => $routes,'cities' => $cities, 'id' =>  $request->city));
        }else if($request['filter'] == 3){

            if($request['city'] == null){
                $routes = Route::get()->sortByDesc('rating');
            }else{
                $routes = City::find( $request->city)->routes->sortByDesc('rating');
            }
            $cities = City::get();
            return view('routes/displayRoutes', array('routes' => $routes,'cities' => $cities, 'id' =>  $request->city));
        }
        else{

            $routes = Route::get();  
            $cities = City::get();
       
            
            return view('routes/displayRoutes', array('routes' => $routes,'cities' => $cities, 'id' => null));
        }

    }
}
