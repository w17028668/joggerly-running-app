<?php

namespace App\Http\Controllers;

use App\Flag;
use Illuminate\Http\Request;

class FlagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $reviewFlags = Flag::where('route_id', null)->get();
        
        $routeFlags = Flag::where('review_id', null)->get();

        return view('flags', array('routeFlags' => $routeFlags, 'reviewFlags' => $reviewFlags));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        Flag::create($request->all());

        return back()->with('message', 'Your route was created successfully!');    
    }

}
