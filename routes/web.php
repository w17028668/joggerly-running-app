<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('home');

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    Route::get('/route/create', 'RouteController@index')->name('create.route')->middleware('verified');
    Route::get('/route', 'RouteController@displayRoutes')->name('display.routes');
    Route::get('/route/{route}', 'RouteController@show')->name('show.route');
    Route::get('/route/{route}/edit', 'RouteController@edit')->name('edit.route')->middleware('verified');
    Route::get('/flags', 'FlagController@index')->name('index.flags');
    Route::post('/route/{route}/delete', 'RouteController@destroy')->name('delete.route')->middleware('verified');
    Route::post('/route/{route}/edit', 'RouteController@update')->name('edit.route.post')->middleware('verified');
    Route::post('/route/{route}/create', 'ReviewController@create')->name('create.review')->middleware('verified');
    Route::post('/route', 'RouteController@getRoutes')->name('get.route');
    Route::post('/route/filter', 'RouteController@filterRoutes')->name('filter.route');
    Route::post('/route/create', 'RouteController@create')->name('create.route.post')->middleware('verified');
    Route::post('/route/{route}', 'FlagController@create')->name('create.flag')->middleware('verified');
});

Route::get('/profile', 'ProfileController@index')->name('profile')->middleware('auth');

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('/users', 'AdminController@users')->name('admin.users');
    Route::get('/users/{user}', 'AdminController@user')->name('admin.user');
    Route::get('/users/{user}/password', 'AdminController@managePassword')->name('admin.user.password');
    Route::post('/users/{user}/update', 'AdminController@changePassword')->name('admin.user.password.change');
    Route::post('/users/{user}/verify', 'AdminController@verify')->name('admin.user.verify');
    Route::delete('/users/{user}/delete', 'AdminController@delete')->name('admin.user.delete');
    Route::post('/users/{user}/suspend', 'AdminController@suspend')->name('admin.user.suspend');
    Route::post('/users/{user}/role', 'AdminController@changeRole')->name('admin.user.change_role');
    Route::get('/users/{user}/email', 'AdminController@emailIndex')->name('admin.email');
    Route::post('/users/{user}/email/compose', 'AdminController@emailCompose')->name('admin.email.compose');
    Route::get('/flags', 'AdminController@getFlags')->name('admin.flags');
});
    
