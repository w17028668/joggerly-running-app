<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Route;
use App\Review;

class Flag extends Model
{
    //
    protected $fillable = [
        'route_id', 'review_id'
      ];


    public function route()
    {
      return $this->belongsTo(Route::class);
    }

    
    public function review()
    {
      return $this->belongsTo(Review::class);
    }
}
