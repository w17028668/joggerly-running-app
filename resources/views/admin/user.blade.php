@extends('layouts.app')
@section('content')
<div class="container" style="padding-bottom: 25px;">
    <nav class="navbar navbar-expand-lg navbar-light bg-admin">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('admin.users') }}">Users <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.flags') }}" class="nav-link">Flags</a>
                    </li>
                </ul>
            </div>
    </nav>
</div>

<div class="container">
    <a href="{{ route('admin.users') }}" class="btn btn-success btn-sm"><i class="fas fa-arrow-left"></i> Back</a>
    <div class="float-right admin-user-section" style="display: flex;">
        <a class="btn btn-warning btn-sm" href="{{ route('admin.user.password', ['user' => $user->id]) }}">
            Manage password
        </a>
        @if ($user->id != Auth::user()->id)
            <a href="{{ route('admin.email', ['user' => $user]) }}" class="btn btn-secondary btn-sm">Send Email</a>
            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-confirm">Delete</button>
            
            @if($user->verified === 1)
            <form id="suspend-form" method="post" action="{{ route('admin.user.suspend', ['user' => $user]) }}">
                @csrf
                <button class="btn btn-danger btn-sm" type="submit">Suspend account</button>  
            </form>
            @endif
        @endif
    </div>
    <hr>
    <h4>{{ $user['name']}} profile</h4>
    <p>
        @if(!$user['verified'])
        <div class="alert alert-light" role="alert">
           This user is not verified or suspended and is not able to use all privileges. If the profile is appropriate, please verify.
           <form id="verifyForm" method="post" action="{{ route('admin.user.verify', ['user' => $user]) }}">
                @csrf
                <a href="#" onclick="document.getElementById('verifyForm').submit();"><b>Verify!</b></a>
            </form>
        </div>
        @endif

        @if (session('status') == 'OK')
            <div class="alert alert-success">
                User <b>{{ $user->name}}</b> was successfully verified!
            </div>
        @endif

        @if (session('status') == 'EMAIL_SENT')
            <div class="alert alert-success">
                Email was sent successfully to user <b>{{ $user->name}}</b>!
            </div>
        @endif

        @if (session('status') == 'SUSPENDED')
            <div class="alert alert-success">
                User <b>{{ $user->name}}</b> was suspended successfully!
            </div>
        @endif

        
        @if (session('status') == 'ROLE_CHANGED')
            <div class="alert alert-success">
                User <b>{{ $user->name}}</b> role was changed successfully!
            </div>
        @endif

        @if (session('status') == 'DELETE_FAIL')
            <div class="alert alert-danger">
                You can't delete your own user.
            </div>     
        @endif

        <div class="user-description">
            <i class="fa fa-envelope"></i> Email:  <b>{{ $user['email'] }}</b><br>
            <i class="fa fa-key"></i> 
                Role:   
                
                @if($user->role === 0)
                    <div class="badge badge-warning">User</div>
                @elseif($user->role === 1)
                    <div class="badge badge-success">Administrator</div>
                @else
                    <div class="badge badge-info">Committee member</div>
                @endif

                @if ($user->id != Auth::user()->id)
                <form id="change-role-form" method="post" action="{{ route('admin.user.change_role', ['user' => $user]) }}">
                    @csrf
                    
                    <select class="form-control" name="role" style="width: 200px;">
                        <option value="0">User</option>
                        <option value="2">Committee member</option>
                        <option value="1">Administrator</option>
                    </select>

                    <button class="btn btn-primary btn-sm" type="submit">Change role</button>  
                </form>
                @endif
            <br>
            <i class="fas fa-calendar-alt"></i> Joined:  <b>{{ $user['created_at'] }}</b><br>
        </div>
    </p>

    <hr>

    <div id="delete-confirm" class="modal fade" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Confirmation</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete user <b>{{ $user->name }}</b> ?</p>
                </div>
                <div class="modal-footer">
                    <form method="POST" action="{{ route('admin.user.delete', ['user' => $user ]) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Yes, delete it.</button>
                    </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
